import numpy as np
from pathlib import Path
import re
import matplotlib.pyplot as plt

def main():
	n_wigner = 99
	n_frequencies = 1000
	fwhm = 0.3

	outputs = get_outputs(n_wigner)

	(energies, intensities) = get_energies_and_intensities(outputs)

	sampled_energies = np.linspace(2, 11, n_frequencies)
	sampled_intensities = np.zeros_like(sampled_energies)

	sigma = fwhm2sigma(fwhm)

	for n in range(n_wigner):
		energies_n = energies[n]
		intensities_n = intensities[n]
		for e in range(len(energies_n)):
			for i in range(len(sampled_energies)):
				de = energies_n[e] - sampled_energies[i]
				sampled_intensities[i] = sampled_intensities[i] + intensities_n[e] / np.sqrt(2 * np.pi * sigma) * np.exp(-de**2/(2 * sigma**2))

	plt.plot(sampled_energies, sampled_intensities/np.max(sampled_intensities))

	for n in range(n_wigner):
		plt.bar(energies[n], intensities[n], width=0.01, color='gray')

	plt.show()

def get_outputs(n):
	outputs = list()
	for ic in range(n):
		outputs.append(Path(f"eT_osc_IC_{ic+1}.out"))
	return outputs

def get_energies_and_intensities(outputs):
	energies = list()
	intensities = list()

	for o in outputs:
		(o_energies, o_intensities) = get_energies_and_intensities_for_output(o)
		energies.append(o_energies)
		intensities.append(o_intensities)

	return (energies, intensities)

def get_energies_and_intensities_for_output(o):
	file = open(o, "r")
	lines = file.readlines()
	file.close()

	energies = list()
	intensities = list()

	for k in range(len(lines)):
		if re.match(r"\s*States n \= 0 and", lines[k]):
			omega = np.float64(lines[k+4].split()[4])
			strength = np.float64(lines[k+15].split()[4])
			energies.append(omega)
			intensities.append(strength)

	return (energies, intensities)

def fwhm2sigma(fwhm):
     return fwhm / np.sqrt(8 * np.log(2))

if __name__ == '__main__':
	main()