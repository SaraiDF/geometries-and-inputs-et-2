#!/bin/bash

for ((t=11; t<100; t++)); do
        
	cp eT.inp eT_osc_IC_${t}.inp
	grep -r -a15 "UNITS=BOHR" wigner_sample_${t}.dat | tail -14 | tee -a eT_osc_IC_${t}.inp
	python3 /home/eirikk/prog/eT/build/eT_launch.py --omp 20 --scratch /scratch/eirikk/eT2/psb-uv-wigner-ccsd-adz -ks eT_osc_IC_$t.inp 

done

