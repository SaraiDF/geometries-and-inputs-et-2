from abc import ABC, abstractmethod
import numpy as np


class broadener(ABC):
    @abstractmethod
    def broaden(self, positions, heights, first, last, normalize=True):
        """
        Receives bars at given positions and with certain heights
        and returns arrays x, y that contain the discretized convolution
        of the bars with some function (e.g. Lorenzian or Gaussian).

        positions : array of positions where Lorenzian will be centered at
        heights   : array of heights
        x_range   : List with two elements determining first and last point on x-axis
        normalize : Scale largest peak to value of 1

        returns arrays x and y corresponding to points to plot.
        """
        pass

    def get_fwhm_string(self):
        return f"{self.fwhm:.1f}"

    def get_fwhm(self):
        return self.fwhm


class lorentzian_broadener(broadener):
    def __init__(self, fwhm: float):
        self.fwhm = fwhm
        if self.fwhm < 0.0:
            raise ValueError

    def broaden(self, positions, heights, first, last, normalize=True):
        x = np.linspace(first, last, 1000)
        y = np.zeros_like(x)

        factor = 2.0 / self.fwhm

        for position, height in zip(positions, heights):

            x_rel = position - x

            y += height / (1.0 + (x_rel * factor) ** 2)

        if normalize:
            max_y = np.amax(y)
            y = y / max_y

        return x, y


class scaling_lorentzian_broadener(broadener):
    """
    Instead of using a constant fwhm scale it linearly from
    fwhm[0] to fwhm[1] between the x-values scaling_range[0], scaling_range[1].
    For values below scaling_range[0] use fwhm[0] and
    for values above scaling_range[1] use fwhm[1]

    Theoretical approximations to X-ray absorption spectroscopy of liquid water and ice
    """

    def __init__(self, fwhm, scaling_range):
        self.fwhm = fwhm
        if any(i < 0 for i in fwhm):
            raise ValueError

        self.scaling_range = scaling_range
        if scaling_range[0] > scaling_range[1]:
            raise ValueError

    def get_fwhm_string(self):
        fwhm_string = f"{self.fwhm[0]:3.1f}-{self.fwhm[1]:3.1f}_from_"
        fwhm_string += f"{self.scaling_range[0]:.1f}-{self.scaling_range[1]:.1f}"
        return fwhm_string

    def get_fwhm(self):
        return self.fwhm[0]

    def broaden(self, positions, heights, first, last, normalize=True):
        print(first)
        print(last)
        x = np.linspace(first, last, 1000)
        y = np.zeros_like(x)

        slope = (self.fwhm[1] - self.fwhm[0]) / (
            self.scaling_range[1] - self.scaling_range[0]
        )

        for position, height in zip(positions, heights):

            if position < self.scaling_range[0]:
                fwhm = self.fwhm[0]
            elif position > self.scaling_range[1]:
                fwhm = self.fwhm[1]
            else:
                fwhm = slope * (position - self.scaling_range[0]) + self.fwhm[0]

            factor = 2.0 / fwhm

            x_rel = position - x

            y += height / (1.0 + (x_rel * factor) ** 2)

        if normalize:
            max_y = np.amax(y)
            y = y / max_y

        return x, y


class gaussian_broadener(broadener):
    def __init__(self, fwhm: float):
        self.fwhm = fwhm
        if self.fwhm < 0.0:
            raise ValueError

    def broaden(self, positions, heights, first, last, normalize=True):
        """Gaussian profile usually defined in terms of standard deviation, σ,
            g(x) = 1/(σ sqrt(2π)) exp[- (x - x0)^2 / (2σ^2)]

        The fwhm is related to the standard deviation:
            fwhm = σ 2 sqrt(2 ln(2))

        We therefore implement
            g(x) = 2/fwhm sqrt(ln(2)/π)) exp[-(x - x0)^2 ln(2) (2/fwhm)^2]
        """
        x = np.linspace(first, last, 1000)
        y = np.zeros_like(x)

        one_over_hwhm = 2 / self.fwhm
        factor_normalization = np.sqrt(np.log(2.0) / np.pi) * one_over_hwhm
        factor_exponent = -np.log(2.0) * one_over_hwhm**2

        for position, height in zip(positions, heights):

            x_rel = x - position

            y += height * factor_normalization * np.exp(factor_exponent * x_rel**2)

        if normalize:
            max_y = np.amax(y)
            y = y / max_y

        return x, y


class scaling_gaussian_broadener(broadener):
    """
    Instead of using a constant fwhm scale it linearly from
    fwhm[0] to fwhm[1] between the x-values scaling_range[0], scaling_range[1].
    For values below scaling_range[0] use fwhm[0] and
    for values above scaling_range[1] use fwhm[1]
    """

    def __init__(self, fwhm, scaling_range):
        self.fwhm = fwhm
        if any(i < 0 for i in fwhm):
            raise ValueError

        self.scaling_range = scaling_range
        if scaling_range[0] > scaling_range[1]:
            raise ValueError

    def get_fwhm_string(self):
        fwhm_string = f"{self.fwhm[0]:3.1f}-{self.fwhm[1]:3.1f}_from_"
        fwhm_string += f"{self.scaling_range[0]:.1f}-{self.scaling_range[1]:.1f}"
        return fwhm_string

    def get_fwhm(self):
        return self.fwhm[0]

    def broaden(self, positions, heights, first, last, normalize=True):
        x = np.linspace(first, last, 1000)
        y = np.zeros_like(x)

        log2 = np.log(2.0)
        sqrt_log2_by_pi = np.sqrt(np.log(2.0) / np.pi)

        slope = (self.fwhm[1] - self.fwhm[0]) / (
            self.scaling_range[1] - self.scaling_range[0]
        )

        for position, height in zip(positions, heights):

            if position < self.scaling_range[0]:
                fwhm = self.fwhm[0]
            elif position > self.scaling_range[1]:
                fwhm = self.fwhm[1]
            else:
                fwhm = slope * (position - self.scaling_range[0]) + self.fwhm[0]

            x_rel = x - position
            factor = -log2 * (2.0 / fwhm) ** 2

            one_over_hwhm = 2 / fwhm
            factor_normalization = sqrt_log2_by_pi * one_over_hwhm
            factor_exponent = -log2 * one_over_hwhm**2

            y += height * factor_normalization * np.exp(factor_exponent * x_rel**2)

        if normalize:
            max_y = np.amax(y)
            y = y / max_y

        return x, y


class voigt_broadener(broadener):
    """
    Broaden using Voigt functions as described here:
    https://en.wikipedia.org/wiki/Voigt_profile
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.special.voigt_profile.html
    """

    def __init__(self, lorentzian_fwhm: float, standard_deviation: float):
        self.lorentzian_fwhm = lorentzian_fwhm
        self.standard_deviation = standard_deviation
        self.fwhm = self.calculate_approximate_fwhm()
        if self.fwhm < 0.0:
            raise ValueError

    def calculate_approximate_fwhm(self):
        """
        Approximate width of the Voigt profile according to
        https://en.wikipedia.org/wiki/Voigt_profile#The_width_of_the_Voigt_profile
        """
        square_fwhm_gaussian = 8 * self.standard_deviation**2 * np.log(2)
        fwhm_voigt = 0.5346 * self.lorentzian_fwhm + np.sqrt(
            0.2166 * self.lorentzian_fwhm**2 + square_fwhm_gaussian
        )
        print(f"Voigt profile approximate FWHM: {fwhm_voigt:.4f}")
        return fwhm_voigt

    def broaden(self, positions, heights, first, last, normalize=True):
        from scipy.special import voigt_profile

        x = np.linspace(first, last, 1000)
        y = np.zeros_like(x)
        hwhm = self.lorentzian_fwhm / 2

        for position, height in zip(positions, heights):

            x_rel = x - position
            y += height * voigt_profile(x_rel, self.standard_deviation, hwhm)

        if normalize:
            max_y = np.amax(y)
            y = y / max_y

        return x, y


class scaling_voigt_broadener(broadener):
    """
    Broaden using Voigt functions as described here:
    https://en.wikipedia.org/wiki/Voigt_profile
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.special.voigt_profile.html
    """

    def __init__(
        self,
        lorentzian_fwhm: float,
        std_range: tuple[float, float],
        scaling_range: tuple[float, float],
        std_outside: tuple[float, float] = None,
    ):
        self.lorentzian_fwhm = lorentzian_fwhm
        if self.lorentzian_fwhm < 0:
            raise ValueError

        self.std_range = std_range

        self.scaling_range = scaling_range
        if scaling_range[0] > scaling_range[1]:
            raise ValueError

        if std_outside is None:
            self.std_outside = self.std_range
        else:
            self.std_outside = std_outside

        self.fwhms = [
            self.calculate_approximate_fwhm(self.lorentzian_fwhm, self.std_range[0]),
            self.calculate_approximate_fwhm(self.lorentzian_fwhm, self.std_range[1]),
        ]

    def get_fwhm_string(self):
        fwhm_string = f"{self.fwhms[0]:3.1f}-{self.fwhms[1]:3.1f}_from_"
        fwhm_string += f"{self.scaling_range[0]:.1f}-{self.scaling_range[1]:.1f}"
        return fwhm_string

    def get_fwhm(self):
        return self.fwhms[0]

    def calculate_approximate_fwhm(self, fwhm, std):
        """
        Approximate width of the Voigt profile according to
        https://en.wikipedia.org/wiki/Voigt_profile#The_width_of_the_Voigt_profile
        """
        square_fwhm_gaussian = 8 * std**2 * np.log(2)
        fwhm_voigt = 0.5346 * fwhm + np.sqrt(0.2166 * fwhm**2 + square_fwhm_gaussian)
        print(f"Voigt profile approximate FWHM: {fwhm_voigt:.4f}")
        return fwhm_voigt

    def broaden(self, positions, heights, first, last, normalize=True):
        from scipy.special import voigt_profile

        x = np.linspace(first, last, 1000)
        y = np.zeros_like(x)
        hwhm = self.lorentzian_fwhm / 2

        slope = (self.std_range[1] - self.std_range[0]) / (
            self.scaling_range[1] - self.scaling_range[0]
        )

        for position, height in zip(positions, heights):

            if position < self.scaling_range[0]:
                std = self.std_outside[0]
            elif position > self.scaling_range[1]:
                std = self.std_outside[1]
            else:
                std = slope * (position - self.scaling_range[0]) + self.std_range[0]

            x_rel = x - position
            y += height * voigt_profile(x_rel, std, hwhm)

        if normalize:
            max_y = np.amax(y)
            y = y / max_y

        return x, y
