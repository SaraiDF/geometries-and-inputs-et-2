from pathlib import Path
import argparse
import numpy as np
import matplotlib.pyplot as plt
from broadening import (
    broadener,
    lorentzian_broadener,
    gaussian_broadener,
)


def extract_eT_exci_file(eT_file, file_name):
    """
    Reads an eT.out type file, and makes a csv file
    with the excitation energies and oscillator strengths
    """
    lines = Path(eT_file).read_text().split("\n")

    energies = []
    intensities = []

    for n, line in enumerate(lines):
        line = line.strip()
        if "Excitation energy (n->m) [eV]:" in line:
            energy = line.split()[-1]
            energies.append(energy)

        if "Oscillator strength (length gauge):" in line:
            strength = line.split()[-1]
            intensities.append(strength)

    contents = [f"{i}, {j}" for i, j in zip(energies, intensities)]
    contents = "Energy [eV],      Intensity\n" + "\n".join(contents)

    Path(file_name).write_text(contents)


def extract_eT_ip_file(eT_file, file_name):
    """
    Reads an eT.out type file, and makes a csv file
    with the excitation energies and oscillator strengths
    """
    lines = Path(eT_file).read_text().split("\n")

    energies = []
    intensities = []

    for n, line in enumerate(lines):
        line = line.strip()
        if "State    IP [eV]    Left*Right" in line:
            i = 2
            new_line = lines[n+i]
            while "--------------------------------" not in new_line:
                energy = new_line.split()[-2]
                intensity = new_line.split()[-1]

                energies.append(energy)
                intensities.append(intensity)
                i += 1
                new_line = lines[n+i]

    contents = [f"{i}, {j}" for i, j in zip(energies, intensities)]
    contents = "Energy [eV],      Intensity\n" + "\n".join(contents)

    Path(file_name).write_text(contents)



def generate_ip_csv_files(path: Path):
    path = path.resolve()
    print(f"Extracting files in: {str(path)}")
    for out_file in path.glob("*.out"):
        if (
            out_file.name.startswith("slurm")
            or out_file.name.endswith("timing.out")
            or out_file.name.startswith("nohup")
            or out_file.name.endswith("dyson_orbitals.out")
        ):
            continue
        csv_file = out_file.with_suffix(".csv")
        extract_eT_ip_file(out_file, csv_file)

def generate_exci_csv_files(path: Path):
    path = path.resolve()
    print(f"Extracting files in: {str(path)}")
    for out_file in path.glob("*.out"):
        if (
            out_file.name.startswith("slurm")
            or out_file.name.endswith("timing.out")
            or out_file.name.startswith("nohup")
            or out_file.name.endswith("dyson_orbitals.out")
        ):
            continue
        csv_file = out_file.with_suffix(".csv")
        extract_eT_exci_file(out_file, csv_file)


def plot_average_spectrum(
    path_to_exci: Path,
    path_to_ioni: Path,
    figure_path: Path,
    broadener: broadener,
    shift,
    limits,
):
    """
    Plots average of spectra (assuming number of states is the same)
    """
    f, ax = plt.subplots(2, 1, sharex=True)

    model_color = ['C0', 'C1']
    process_style = ['-', ':']
    labels = ['ES', 'IP']

    generate_exci_csv_files(path_to_exci)
    generate_ip_csv_files(path_to_ioni)

    for j, model in enumerate(['ccsd', 'cc3']):
        for i, process_path in enumerate([path_to_exci, path_to_ioni]):

            path = process_path.resolve()
            for file in path.glob(f"*{model}*.csv"):
                e, o = np.loadtxt(file, skiprows=1, unpack=True, delimiter=",")
                max_o = np.amax(o)
                o = o/max_o


                e += shift[j]
                energies = e.flatten()

                min_x = np.amin(energies) - 10 * broadener.get_fwhm()
                max_x = np.amax(energies) + 10 * broadener.get_fwhm()

                x, y = broadener.broaden(energies, o.flatten(), min_x, max_x, normalize=False)

           ## Plot average for total oscillator strength and normalize by matching the integral
                shift_str = f"{shift[j]:.2f}"
                label = f"{labels[i]} ({shift_str} eV shift)"

                ax[j].plot(x, y, process_style[i], color=model_color[j], label=label, zorder=2, lw=2.2)

    # Specifications for the figure
    ax[1].legend(loc='upper right')
    ax[0].legend(loc='upper right')
    ax[1].set_xlabel("Energy [eV]")
    ax[0].set_xlim([limits[0], limits[1]])
    ax[0].set_ylim(-0.05, 1.05)
    ax[0].set_ylabel("Intensity [arb. units]")
    ax[1].set_ylabel("Intensity [arb. units]")
    #ax.set_yticks([0, 0.5, 1, 1.5])

    plt.subplots_adjust(left=0.115, bottom=0.12, right=0.968, top=0.99)
    f.savefig(figure_path)





broadener = lorentzian_broadener(0.4)

plot_average_spectrum(
    Path('../s0-uv/'),
    Path('../s0-ip/'),
    Path('../figures/test.png'),
    broadener,
    [-1.5, 0],
    [1,20]
)

# plot_average_spectrum(
#     Path('../s0-nexafs/'),
#     Path('../s0-xps/'),
#     Path('../figures/N_core.png'),
#     broadener,
#     [-1.5, 0],
#     [401,422]
# )



