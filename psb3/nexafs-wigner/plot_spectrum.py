from pathlib import Path
import re 
import numpy as np
import matplotlib.pyplot as plt

def main():
   # IC_range = [11,21,31,41,51,61,71,81,91,101]
    IC_range = [40]
    for IC_max in IC_range:
     print (IC_max)
     intensities_tot = 0.0
     energies_tot = []
     oscillators_tot = []
     for num_IC in range(1,IC_max):
        lines = get_lines("eT_osc_IC_"+str(num_IC)+".out")
        shift = 1.5e0
     #   shift = 0.35    #  shift without augmentation
     #   shift = 0.22     #  shift with augmentation
        (energies_s1, oscillators_s1) = extract_transitions(1, lines, shift)
        (energies_s2, oscillators_s2) = extract_transitions(2, lines, shift)
        (energies_s3, oscillators_s3) = extract_transitions(3, lines, shift)
        (energies_s4, oscillators_s4) = extract_transitions(4, lines, shift)
        (energies_s5, oscillators_s5) = extract_transitions(5, lines, shift)
        (energies_s6, oscillators_s6) = extract_transitions(6, lines, shift)
        (energies_s7, oscillators_s7) = extract_transitions(7, lines, shift)
        (energies_s8, oscillators_s8) = extract_transitions(8, lines, shift)
        (energies_s9, oscillators_s9) = extract_transitions(9, lines, shift)
        (energies_s10, oscillators_s10) = extract_transitions(10, lines, shift)
        (energies_s11, oscillators_s11) = extract_transitions(11, lines, shift)
        (energies_s12, oscillators_s12) = extract_transitions(12, lines, shift)
        (energies_s13, oscillators_s13) = extract_transitions(13, lines, shift)
        (energies_s14, oscillators_s14) = extract_transitions(14, lines, shift)
        (energies_s15, oscillators_s15) = extract_transitions(15, lines, shift)
        (energies_s16, oscillators_s16) = extract_transitions(16, lines, shift)
        (energies_s17, oscillators_s17) = extract_transitions(17, lines, shift)
        (energies_s18, oscillators_s18) = extract_transitions(18, lines, shift)
        (energies_s19, oscillators_s19) = extract_transitions(19, lines, shift)
        (energies_s20, oscillators_s20) = extract_transitions(20, lines, shift)
        (energies_s21, oscillators_s21) = extract_transitions(21, lines, shift)
        (energies_s22, oscillators_s22) = extract_transitions(22, lines, shift)
        (energies_s23, oscillators_s23) = extract_transitions(23, lines, shift)
        (energies_s24, oscillators_s24) = extract_transitions(24, lines, shift)
        (energies_s25, oscillators_s25) = extract_transitions(25, lines, shift)
        (energies_s26, oscillators_s26) = extract_transitions(26, lines, shift)
        (energies_s27, oscillators_s27) = extract_transitions(27, lines, shift)
        (energies_s28, oscillators_s28) = extract_transitions(28, lines, shift)
        (energies_s29, oscillators_s29) = extract_transitions(29, lines, shift)
        (energies_s30, oscillators_s30) = extract_transitions(30, lines, shift)

        energies_tot.append(energies_s1[0])
        energies_tot.append(energies_s2[0])
        energies_tot.append(energies_s3[0])
        energies_tot.append(energies_s4[0])
        energies_tot.append(energies_s5[0])
        energies_tot.append(energies_s6[0])
        energies_tot.append(energies_s7[0])
        energies_tot.append(energies_s8[0])
        energies_tot.append(energies_s9[0])
        energies_tot.append(energies_s10[0])
        energies_tot.append(energies_s11[0])
        energies_tot.append(energies_s12[0])
        energies_tot.append(energies_s13[0])
        energies_tot.append(energies_s14[0])
        energies_tot.append(energies_s15[0])
        energies_tot.append(energies_s16[0])
        energies_tot.append(energies_s17[0])
        energies_tot.append(energies_s18[0])
        energies_tot.append(energies_s19[0])
        energies_tot.append(energies_s20[0])
        energies_tot.append(energies_s21[0])
        energies_tot.append(energies_s22[0])
        energies_tot.append(energies_s23[0])
        energies_tot.append(energies_s24[0])
        energies_tot.append(energies_s25[0])
        energies_tot.append(energies_s26[0])
        energies_tot.append(energies_s27[0])
        energies_tot.append(energies_s28[0])
        energies_tot.append(energies_s29[0])
        energies_tot.append(energies_s30[0])
        
        oscillators_tot.append(oscillators_s1[0])
        oscillators_tot.append(oscillators_s2[0])
        oscillators_tot.append(oscillators_s3[0])
        oscillators_tot.append(oscillators_s4[0])
        oscillators_tot.append(oscillators_s5[0])
        oscillators_tot.append(oscillators_s6[0])
        oscillators_tot.append(oscillators_s7[0])
        oscillators_tot.append(oscillators_s8[0])
        oscillators_tot.append(oscillators_s9[0])
        oscillators_tot.append(oscillators_s10[0])
        oscillators_tot.append(oscillators_s11[0])
        oscillators_tot.append(oscillators_s12[0])
        oscillators_tot.append(oscillators_s13[0])
        oscillators_tot.append(oscillators_s14[0])
        oscillators_tot.append(oscillators_s15[0])
        oscillators_tot.append(oscillators_s16[0])
        oscillators_tot.append(oscillators_s17[0])
        oscillators_tot.append(oscillators_s18[0])
        oscillators_tot.append(oscillators_s19[0])
        oscillators_tot.append(oscillators_s20[0])
        oscillators_tot.append(oscillators_s21[0])
        oscillators_tot.append(oscillators_s22[0])
        oscillators_tot.append(oscillators_s23[0])
        oscillators_tot.append(oscillators_s24[0])
        oscillators_tot.append(oscillators_s25[0])
        oscillators_tot.append(oscillators_s26[0])
        oscillators_tot.append(oscillators_s27[0])
        oscillators_tot.append(oscillators_s28[0])
        oscillators_tot.append(oscillators_s29[0])
        oscillators_tot.append(oscillators_s30[0])
        #plt.plot(energies_s0[2:], oscillators_s0[2:], '.k')
        #plt.plot(energies_s1[1:], oscillators_s1[1:], '.r')
        #plt.plot(energies_s2[0:], oscillators_s2[0:], '.b')
        #plt.show()

        fwhm = 0.15 # eV
        (frequencies_s1, intensities_s1) = do_gaussian_broadening(energies_s1, oscillators_s1, fwhm)
        (frequencies_s2, intensities_s2) = do_gaussian_broadening(energies_s2, oscillators_s2, fwhm)
        (frequencies_s3, intensities_s3) = do_gaussian_broadening(energies_s3, oscillators_s3, fwhm)
        (frequencies_s4, intensities_s4) = do_gaussian_broadening(energies_s4, oscillators_s4, fwhm)
        (frequencies_s5, intensities_s5) = do_gaussian_broadening(energies_s5, oscillators_s5, fwhm)
        (frequencies_s6, intensities_s6) = do_gaussian_broadening(energies_s6, oscillators_s6, fwhm)
        (frequencies_s7, intensities_s7) = do_gaussian_broadening(energies_s7, oscillators_s7, fwhm)
        (frequencies_s8, intensities_s8) = do_gaussian_broadening(energies_s8, oscillators_s8, fwhm)
        (frequencies_s9, intensities_s9) = do_gaussian_broadening(energies_s9, oscillators_s9, fwhm)
        (frequencies_s10, intensities_s10) = do_gaussian_broadening(energies_s10, oscillators_s10, fwhm)
        (frequencies_s11, intensities_s11) = do_gaussian_broadening(energies_s11, oscillators_s11, fwhm)
        (frequencies_s12, intensities_s12) = do_gaussian_broadening(energies_s12, oscillators_s12, fwhm)
        (frequencies_s13, intensities_s13) = do_gaussian_broadening(energies_s13, oscillators_s13, fwhm)
        (frequencies_s14, intensities_s14) = do_gaussian_broadening(energies_s14, oscillators_s14, fwhm)
        (frequencies_s15, intensities_s15) = do_gaussian_broadening(energies_s15, oscillators_s15, fwhm)
        (frequencies_s16, intensities_s16) = do_gaussian_broadening(energies_s16, oscillators_s16, fwhm)
        (frequencies_s17, intensities_s17) = do_gaussian_broadening(energies_s17, oscillators_s17, fwhm)
        (frequencies_s18, intensities_s18) = do_gaussian_broadening(energies_s18, oscillators_s18, fwhm)
        (frequencies_s19, intensities_s19) = do_gaussian_broadening(energies_s19, oscillators_s19, fwhm)
        (frequencies_s20, intensities_s20) = do_gaussian_broadening(energies_s20, oscillators_s20, fwhm)
        (frequencies_s21, intensities_s21) = do_gaussian_broadening(energies_s21, oscillators_s21, fwhm)
        (frequencies_s22, intensities_s22) = do_gaussian_broadening(energies_s22, oscillators_s22, fwhm)
        (frequencies_s23, intensities_s23) = do_gaussian_broadening(energies_s23, oscillators_s23, fwhm)
        (frequencies_s24, intensities_s24) = do_gaussian_broadening(energies_s24, oscillators_s24, fwhm)
        (frequencies_s25, intensities_s25) = do_gaussian_broadening(energies_s25, oscillators_s25, fwhm)
        (frequencies_s26, intensities_s26) = do_gaussian_broadening(energies_s26, oscillators_s26, fwhm)
        (frequencies_s27, intensities_s27) = do_gaussian_broadening(energies_s27, oscillators_s27, fwhm)
        (frequencies_s28, intensities_s28) = do_gaussian_broadening(energies_s28, oscillators_s28, fwhm)
        (frequencies_s29, intensities_s29) = do_gaussian_broadening(energies_s29, oscillators_s29, fwhm)
        (frequencies_s30, intensities_s30) = do_gaussian_broadening(energies_s30, oscillators_s30, fwhm)

        intensities_tot = intensities_tot + intensities_s1 + intensities_s2 + intensities_s3 + intensities_s4 + intensities_s5 \
            + intensities_s6 + intensities_s7 + intensities_s8 + intensities_s9 + intensities_s10 + intensities_s11 \
            + intensities_s12 + intensities_s13 + intensities_s14 + intensities_s15 + intensities_s16 + intensities_s17 \
            + intensities_s18 + intensities_s19 + intensities_s20 + intensities_s21 + intensities_s22 + intensities_s23 \
            + intensities_s24 + intensities_s25 + intensities_s26 + intensities_s27 + intensities_s28 + intensities_s29 \
            + intensities_s30 
     intensities_tot = intensities_tot/np.max(intensities_tot)

     plt.clf()
     spectrum_100 = open("spectrum_complete_sample_shift_"+str(shift)+"_eV_FWHM_"+str(fwhm)+"_eV.dat", "w")
    # if IC_max == 101:
    #    print (IC_max)
    #    for i in range(0, len(frequencies_s1)): 
    #       spectrum_100.write(str(frequencies_s1[i])+"       "+str(intensities_tot[i])+"\n")
     plt.bar(energies_tot, oscillators_tot/np.max(oscillators_tot), width=0.02, color='k', alpha=0.15)
     plt.plot(frequencies_s1, intensities_tot, color="C0", linewidth=1.5)
    #    plt.ylim([0,0.05])
     plt.xlim([400,422])
     plt.xlabel("Excitation energy [eV]")
     plt.ylabel("Intensity")
    # plt.legend()
    # plt.savefig("spectra/"+str(basis_set)+"/pyrazine-"+str(basis_set)+"_shift_"+str(shift)+"_eV_FWHM_"+str(fwhm)+"_eV_"+str(IC_max-1)+"_IC.png", dpi=600)
    plt.show()

def do_gaussian_broadening(energies, oscillators, fwhm):
        sigma = fwhm2sigma(fwhm)
        frequencies = np.linspace(395, 425, 1000)
        intensities = np.zeros_like(frequencies)
        for j in np.arange(len(energies)):
            for i in np.arange(len(frequencies)):
                        w = oscillators[j]
                        dx = energies[j] - frequencies[i]
                        intensities[i] = intensities[i] + w / np.sqrt(2 * np.pi * sigma) * np.exp(-dx**2/(2 * sigma**2))
        return (frequencies, intensities)


def get_lines(file_path):
        out_file = open(file_path, "r")
        out_lines = out_file.readlines()
        out_file.close()
        return out_lines

def extract_transitions(final_state, lines, shift):
        energies = list()
        oscillators = list()
        for k in np.arange(len(lines)):
              if re.search(f"m = {final_state}", lines[k]):
                        excitation_energy = np.float64(lines[k+4].split()[4]) - shift
                        oscillator_strength = np.float64(lines[k+15].split()[4])
                        energies.append(excitation_energy)
                        oscillators.append(oscillator_strength)

        return (energies, oscillators)

def fwhm2sigma(fwhm):
     return fwhm / np.sqrt(8 * np.log(2))

if __name__ == "__main__":
     main()
