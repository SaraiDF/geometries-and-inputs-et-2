#!/bin/bash

for ((t=1; t<100; t++)); do
        
	cp eT.inp eT_osc_IC_${t}.inp
	grep -r -a15 "UNITS=BOHR" ../uv-wigner/wigner_sample_${t}.dat | tail -14 | tee -a eT_osc_IC_${t}.inp
	python3 /home/eirikk/prog/eT/build/eT_launch.py --omp 8 --scratch /scratch/eirikk/eT2/psb-nexafs-wigner-ccsd-adz -ks eT_osc_IC_$t.inp 

done

